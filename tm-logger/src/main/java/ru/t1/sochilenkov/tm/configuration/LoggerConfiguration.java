package ru.t1.sochilenkov.tm.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.sochilenkov.tm")
public class LoggerConfiguration {

    @Bean
    public ConnectionFactory factory() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory();
        factory.setBrokerURL(System.getenv().containsKey("JMS_BROKERURL") ? System.getenv("JMS_BROKERURL") : "tcp://tm-server-alpha:61616");
        factory.setTrustAllPackages(true);
        return factory;
    }

}
