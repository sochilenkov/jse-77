package ru.t1.sochilenkov.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.t1.sochilenkov.tm.model.Project;

@Repository
public interface IProjectRepository extends IUserOwnedRepository<Project> {
}
