package ru.t1.sochilenkov.tm.repository.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.sochilenkov.tm.dto.UserDTO;

@Repository
public interface IUserDTORepository extends JpaRepository<UserDTO, String> {

    @Nullable
    UserDTO findByLogin(final String login);

}
