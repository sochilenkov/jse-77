package ru.t1.sochilenkov.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.sochilenkov.tm.dto.TaskDTO;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskFindByIdResponse")
public class TaskFindByIdResponse {

    @XmlElement(required = true)
    protected TaskDTO task;

}
