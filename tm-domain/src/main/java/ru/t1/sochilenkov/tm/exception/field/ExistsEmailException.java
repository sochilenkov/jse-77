package ru.t1.sochilenkov.tm.exception.field;

public final class ExistsEmailException extends AbstractFieldException {

    public ExistsEmailException() {
        super("Error! This email already exists...");
    }

}
