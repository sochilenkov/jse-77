package ru.t1.sochilenkov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_user")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(nullable = false, name = "login")
    private String login;

    @Nullable
    @Column(nullable = false, name = "password_hash")
    private String passwordHash = "";

    @Nullable
    @Column(name = "email")
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(nullable = false, name = "role")
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(nullable = false, name = "locked")
    private Boolean locked = false;

}
