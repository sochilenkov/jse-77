package ru.t1.sochilenkov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.dto.model.ProjectDTO;

import java.util.List;

@NoArgsConstructor
public final class ProjectListResponse extends AbstractProjectResponse {

    @Nullable
    @Getter
    @Setter
    List<ProjectDTO> projects;

    public ProjectListResponse(@Nullable final List<ProjectDTO> projects) {
        this.projects = projects;
    }

}
